//
//  NoteViewController.swift
//  Notes
//
//  Created by michal on 24/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import CoreData

class NoteViewController: UITableViewController {

    var notes = [Note]()
    let calendar = Calendar.current
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var selectedFolder: Folder? {
        didSet {
            loadNotes()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)
        
        let date = notes[indexPath.row].date
        
        cell.textLabel?.text = "New Note \(indexPath.row)"
        
        if let additionalDate = date {
            cell.detailTextLabel?.text = showDate(date: additionalDate)
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! PreviewViewController
        
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedNote = notes[indexPath.row]
        }
    }
    
    // MARK: - Table view delegate methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "goToPreview", sender: self)
    }
    
    // MARK: - Add new note
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        let newNote = Note(context: self.context)
        newNote.date = Date()
        newNote.body = ""
        newNote.parentFolder = self.selectedFolder
        
        self.notes.append(newNote)
        self.saveNotes()
        self.tableView.reloadData()
//        performSegue(withIdentifier: "goToPreview", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            context.delete(notes[indexPath.row])
            notes.remove(at: indexPath.row)
            saveNotes()
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)
        }
    }
    
    
    // MARK: - Data manipulation methods
    
    func saveNotes() {
        do {
            try context.save()
        } catch {
            print("Context couldn't be save")
        }
    }
    
    func loadNotes(with request: NSFetchRequest<Note> = Note.fetchRequest(), predicate: NSPredicate? = nil) {
        
        let folderPredicate = NSPredicate(format: "parentFolder.name MATCHES %@", selectedFolder!.name!)
        
        if let additionalPredicate = predicate {
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [folderPredicate, additionalPredicate])
        } else {
            request.predicate = folderPredicate
        }
        
        do {
            notes =  try context.fetch(request)
        } catch {
            print("Error fetching data from context \(error)")
        }
        
        tableView.reloadData()
    }

}
