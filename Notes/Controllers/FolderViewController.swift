//
//  FolderViewController.swift
//  Notes
//
//  Created by michal on 24/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import CoreData

class FolderViewController: UITableViewController {
    
    var folders = [Folder]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadFolders()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folders.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FolderCell", for: indexPath)
        
        cell.textLabel?.text = folders[indexPath.row].name
        
        return cell
    }
    
    // MARK: - Table view delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToNotes", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! NoteViewController
        
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedFolder = folders[indexPath.row]
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            context.delete(folders[indexPath.row])
            folders.remove(at: indexPath.row)
            saveFolders()
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)
        }
    }
    
    // MARK: - Add new folder
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        
        let alert = UIAlertController(title: "New Folder", message: "Enter a name for this folder", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Save", style: .default) { (action) in
            let newFolder = Folder(context: self.context)
            newFolder.name = textField.text!
            self.folders.append(newFolder)
            
            self.saveFolders()
            
            self.tableView.reloadData()
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            action in
            alert.resignFirstResponder()
        }))
        
        alert.addTextField { (field) in
            field.placeholder = "Name"
            textField = field
        }
        
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Edit folder
    
    @IBAction func editButtonPressed(_ sender: UIBarButtonItem) {
        tableView.isEditing = !tableView.isEditing
    }
    
    // MARK: - Data manipulation methods
    
    func saveFolders() {
        do {
            try context.save()
        } catch {
            print("Error saving context")
        }
    }
    
    func loadFolders() {
        let request: NSFetchRequest<Folder> = Folder.fetchRequest()
        
        do {
            folders = try context.fetch(request)
        } catch {
            print("Error fetching data from context")
        }
    }

}
