//
//  PreviewViewController.swift
//  Notes
//
//  Created by michal on 24/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit
import CoreData

class PreviewViewController: UIViewController {

    @IBOutlet weak var bodyTextView: UITextView!
    
    var note = [Note]()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var selectedNote: Note? {
        didSet {
            loadNote()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bodyTextView.text = selectedNote?.body
    }
    
    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem) {

        //if pusto -> usuń notatke

        if let additionalSelectedNote = selectedNote {
            let updateNote = additionalSelectedNote
            updateNote.date = Date()
            updateNote.body = bodyTextView.text
            
            self.note.append(updateNote)
            self.saveNote()
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    func saveNote() {
        do {
            try context.save()
        } catch {
            print("Context couldn't be save")
        }
    }
    
    func loadNote(with request: NSFetchRequest<Note> = Note.fetchRequest(), predicate: NSPredicate? = nil) {

        let notePredicate = NSPredicate(format: "body MATCHES %@", selectedNote!.body!)

        if let additionalPredicate = predicate {
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [notePredicate, additionalPredicate])
        } else {
            request.predicate = notePredicate
        }

        do {
            note =  try context.fetch(request)
        } catch {
            print("Error fetching data from context \(error)")
        }
    }
}
