//
//  NotesExtension.swift
//  Notes
//
//  Created by michal on 24/09/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import Foundation

extension NoteViewController {
    
    func showDate(date: Date) -> String {
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        return "\(hour):\(minutes) \(day).\(month).\(year)"
    }
}
